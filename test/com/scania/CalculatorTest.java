package com.scania;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {


    @Test
   public void testAddition_InputValuesAdd_ReturnsSumOfValues () {
    //AAA, Arrange, Act, Assert

    //Arrange

    int testAddValueOne = 1;
    int testAddValueTwo = 1;
    int expectedSumResult = 2;

    Calculator basicCalc = new Calculator();


    //Act

    int actual = basicCalc.getSum(testAddValueOne, testAddValueTwo);

    //Assert

   assertEquals(expectedSumResult, actual);

    }


    @Test
    public void testSubtraction_InputValuesSubtract_ReturnsDifferenceOfValues () {
        //AAA, Arrange, Act, Assert

        //Arrange

        int testSubValueOne = 1;
        int testSubValueTwo = 1;
        int expectedDifResult = 0;

        Calculator basicCalc = new Calculator();


        //Act

        int actual = basicCalc.getDif(testSubValueOne, testSubValueTwo);

        //Assert

        assertEquals(expectedDifResult, actual);

    }


    @Test
    public void testMultiplication_InputValuesMultiply_ReturnsProductOfValues () {
        //AAA, Arrange, Act, Assert

        //Arrange

        int testValueOne = 1;
        int testValueTwo = 1;
        int expectedProductResult = 1;

        Calculator basicCalc = new Calculator();


        //Act

        int actual = basicCalc.getProduct(testValueOne, testValueTwo);

        //Assert

        assertEquals(expectedProductResult, actual);

    }


    @Test
    public void testDivision_InputValuesDivide_ReturnsQuotientOfValues () throws divideException {
        //AAA, Arrange, Act, Assert

        //Arrange

        int testValueOne = 1;
        int testValueTwo = 1;
        int expectedQuotientResult = 1;

        Calculator basicCalc = new Calculator();


        //Act

        int actual = basicCalc.getQuotient(testValueOne, testValueTwo);

        //Assert

        assertEquals(expectedQuotientResult, actual);

    }

    @Test
    public void testDivisionWithZero_ReturnsException () {

    int testValueOne = 1;
    int testValueTwo = 0;

    Calculator basicCalc = new Calculator();

    assertThrows(divideException.class, () -> Calculator.getQuotient(testValueOne, testValueTwo));

}}

