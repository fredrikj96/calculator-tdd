package com.scania;

public class Calculator {

    public static int getSum (int a, int b) {

        return a + b;
    }

    public static int getDif(int a, int b) {
        return a - b;
    }

    public static int getProduct(int a, int b) {
        return a * b;
    }

    public static int getQuotient(int a, int b) throws divideException {
       if (b==0) {
       throw new divideException();
       }

        return a / b;
    }

}
